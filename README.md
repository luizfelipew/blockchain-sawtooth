# blockchain-sawtooth
Nesse comando é feita a suposição que seu mysql foi instalado com o usuário root e com a senha vazia. Se tiver uma senha de acesso, é só passar como parâmetro. Ex:

```
java -Dspring.datasource.username=root -Dspring.datasource.password=SUASENHAAQUI  -jar webapp-runner.jar --port 8080 --expand-war eleicoesonline.war
```

Gerar perfis do sistema
http://localhost:8080/magic/generate/roles

Criando usuario para realizacao de testes 
http://localhost:8080/magic/generate/owner/

Executando o Sawtooth

Pelo terminal, navegue até a pasta onde você criou o arquivo do compose. De dentro dessa pasta, execute o seguinte comando:

```
docker-compose -f sawtooth-default.yaml up
```

Depois de parar, sempre rode o comando 
```
docker-compose -f sawtooth-default.yaml down 
```
para garantir que a estrutura gerada pelo docker realmente foi 
destruída.

Caso de erro para 
```
npm install
```

Pode usar
```
rm -rf node_modules && npm i --no-optional
``` 